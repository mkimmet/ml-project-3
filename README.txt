#Setup and Environment#

I am using Python with scikit Learn, pandas, numpy, and a few other minor libraries. You
will need to install all the libraries needed.  I used Python 3 for this assignment.

##Files and Data##
My data and python files can be found at this repo:
https://bitbucket.org/mkimmet/ml-project-3/src/master/

##File/Folder Structure##
Files are broken up based on the dataset.  Letters folder is for the letters dataset and the
Phishing folder is for the phishing dataset.  Each have similary names files which perform
the same duties as described below.

##KMeans Clustering##
kmeans-elbow.py -> if you run this you will get my KMeans data and graphs for each dataset

##EM Clustering##
em-elbow.py -> if you run this you will get my EM data and graphs for each dataset

##Dimensionality Reduction##

###PCA###
pca-line.py -> Produces my line charts for pca
pca-bar.py -> Produces my bar charts for pca

###ICA###
ica.py -> produces my ica line chart

###RP###
rp.py -> produces my rp line chart

###SelectKBest###
selectkbest.py -> produces my SelectKBest line chart

##Clustering with Dimensionality Reduction##
reduced-clustering.py -> produces the EM and Kmeans charts for all the dimensionality reduction algorithms

Please note that you have to comment out each algorithm that you want to run and run them one at a time.

##Nueral Network For LETTERS##

###PCA,ICA,RP,SelectKBest###
letters/reduced-output.py ->  This file produces all the files that you need to run the Neural Networks in Weka.

NOTE:
For PCA, ICA, RP, and SelectKBest it produces files named "method"-test.csv for the test data and "method".csv
for the training data.  So for example ICA.csv and ICA-test.csv.

Then you need to convert these files to ARFF files using Weka, and then use Weka with a NN with
default settings to train and test using the data that was created by reduced-output.py.

###Kmeans and EM###
Run the reduced-output.py file which produces KMEANS-test.csv, KMEANS.csv, EM.csv, and EM-test.csv.

Take those files and convert them to ARFF files using Weka.  Then merge those files with training-80P.ARFF
and training-Test-20P.arff (the test data).  This has already been done with the files outputed as 
KMEANS-combined.arff, KMEANS-test-combined.arff, EM-combined.arff, and EM-test-combined.arff.

Then use those combined files to training and test a default NN in Weka.







## NOTES FOR MARK KIMMET ABOUT MY LOCAL INSTALL TAs CAN PLEASE IGNORE##

**Activate the Virtual Environment for Python**
source ml-class/bin/activate

This will make it so you are running in python 3 environment which is setup with scikit learn

**VSCODE**
I changed the VSCode settings.json to point to the python3 ml-class venv so linting would work:


