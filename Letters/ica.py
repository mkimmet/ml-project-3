#https://pythonprogramminglanguage.com/kmeans-elbow-method/

import pandas as pd
import sys
from sklearn.cluster import MiniBatchKMeans, KMeans
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn import datasets
from sklearn import metrics
from scipy.spatial.distance import cdist
import numpy as np
from sklearn import preprocessing
from sklearn.decomposition import FastICA
from scipy.stats import kurtosis

file = './training-80P.csv'

cols = pd.read_csv(file, nrows=1).columns
numcols = len(cols)
#print (numcols)
features = pd.read_csv(file, usecols=cols[:-1])
#print (phish_data)
targets = pd.read_csv(file, usecols=[numcols-1])
#print (phish_target)

features_labels = ['x-box','y-box',
'width',
'height',
'onpix',
'x-bar',
'y-bar',
'x2bar',
'y2bar',
'xybar',
'x2ybar',
'xy2bar',
'x-ege',
'xegvy',
'y-ege',
'yegvx']

target_labels=['A', 'B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']

scaled_features = pd.DataFrame(preprocessing.scale(features),columns = features_labels) 

features = range(1,17)
kurts=[]
iterations=5
for n in features:
    tmp_kurt=[]
    for _ in range(iterations):
        ica = FastICA(n_components=n) 
        X=ica.fit_transform(scaled_features)
        tmp_kurt.append(np.mean(kurtosis(X)**2))
    val=np.mean(tmp_kurt)
    kurts.append(val)
    print(val)

# Plot the elbow
plt.plot(features, kurts, 'bx-')
plt.xlabel('Features')
plt.ylabel('Kurtosis')
plt.title('Letters Data Kurtosis')
plt.show()

