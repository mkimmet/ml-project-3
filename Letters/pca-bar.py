#USED THIS FOR PCA BAR CHART
#https://cmdlinetips.com/2018/03/pca-example-in-python-with-scikit-learn/

import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
from sklearn.datasets import make_blobs
from sklearn import decomposition
from sklearn import preprocessing

file = './training-80P.csv'
cols = pd.read_csv(file, nrows=1).columns
numcols = len(cols)
#print (numcols)
features = pd.read_csv(file, usecols=cols[:-1])
#print (phish_data)
target = pd.read_csv(file, usecols=[numcols-1])
#print (phish_target)

features_names = ['x-box','y-box',
'width',
'height',
'onpix',
'x-bar',
'y-bar',
'x2bar',
'y2bar',
'xybar',
'x2ybar',
'xy2bar',
'x-ege',
'xegvy',
'y-ege',
'yegvx']

targets=['A', 'B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']

print(features.shape)

#scaled_features = pd.DataFrame(preprocessing.scale(features),columns = features_names) 

pca = decomposition.PCA(n_components=16) 
pc = pca.fit_transform(features)

pc_df = pd.DataFrame(data = pc ,
        columns = ['PC1', 'PC2','PC3','PC4','PC5','PC6','PC7','PC8','PC9','PC10','PC11','PC12','PC13','PC14','PC15','PC16'])
pc_df['Cluster'] = target
pc_df.head()

df = pd.DataFrame({'var':pca.explained_variance_ratio_,
             'PC':['P1', 'P2','P3','P4','P5','P6','P7','P8','P9','P10','P11','P12','P13','P14','P15','P16']})
sns.barplot(x='PC',y="var", 
           data=df, color="c")

sns.lmplot( x="PC1", y="PC2",
  data=pc_df, 
  fit_reg=False, 
  hue='Cluster',
  legend=True,
  scatter_kws={"s": 80}) 

print(pca.components_)
plt.title("Letters Dataset PCA")
plt.xlabel("PC")
plt.ylabel("Variance")
plt.show()