#USED FOR EM ELBOW 
import pandas as pd
import sys
from sklearn.mixture import GaussianMixture
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn import datasets
from sklearn import metrics
from scipy.spatial.distance import cdist
import numpy as np
from sklearn import preprocessing
import seaborn as sns; sns.set()
from sklearn.metrics import silhouette_score

def SelBest(arr:list, X:int)->list:
    '''
    returns the set of X configurations with shorter distance
    '''
    dx=np.argsort(arr)[:X]
    return arr[dx]

file = './training-80P.csv'

cols = pd.read_csv(file, nrows=1).columns
numcols = len(cols)
#print (numcols)
features = pd.read_csv(file, usecols=cols[:-1])
#print (phish_data)
targets = pd.read_csv(file, usecols=[numcols-1])
#print (phish_target)

features_labels = ['x-box','y-box',
'width',
'height',
'onpix',
'x-bar',
'y-bar',
'x2bar',
'y2bar',
'xybar',
'x2ybar',
'xy2bar',
'x-ege',
'xegvy',
'y-ege',
'yegvx']

target_labels=['A', 'B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']

scaled_features = pd.DataFrame(preprocessing.scale(features),columns = features_labels) 

#https://github.com/vlavorini/ClusterCardinality/blob/master/Cluster%20Cardinality.ipynb
n_clusters=np.arange(2, 10)
sils=[]
sils_err=[]
iterations=5
for n in n_clusters:
    tmp_sil=[]
    for _ in range(iterations):
        gmm=GaussianMixture(n, n_init=2).fit(scaled_features) 
        labels=gmm.predict(scaled_features)
        sil=metrics.silhouette_score(scaled_features, labels, metric='euclidean')
        tmp_sil.append(sil)
    val=np.mean(SelBest(np.array(tmp_sil), int(iterations/1)))
    err=np.std(tmp_sil)
    sils.append(val)
    sils_err.append(err)
    print(n)
    print(tmp_sil)
    print(val)
    print(err)

plt.errorbar(n_clusters, sils, yerr=sils_err)
plt.title("Letters Silhouette Scores")
plt.xticks(n_clusters)
plt.xlabel("Clusters")
plt.ylabel("Score")
plt.show()