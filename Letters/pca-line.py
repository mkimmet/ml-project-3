#USED TO GENERATE THE PCA LINE CHART
#https://towardsdatascience.com/an-approach-to-choosing-the-number-of-components-in-a-principal-component-analysis-pca-3b9f3d6e73fe

import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
# load make_blobs to simulate data
from sklearn.datasets import make_blobs
# load decomposition to do PCA analysis with sklearn
from sklearn import decomposition
from sklearn import preprocessing

from sklearn.decomposition import PCA
from sklearn.preprocessing import MinMaxScaler

filepath = './training-80P.csv'
features = np.genfromtxt(filepath, delimiter=',', dtype='float64')
scaler = MinMaxScaler(feature_range=[0, 1])
scaled_features = scaler.fit_transform(features[1:, 0:16])

pca = PCA().fit(scaled_features)

#Plot
plt.figure()
plt.plot(np.cumsum(pca.explained_variance_ratio_))
plt.xlabel('Number of Features')
plt.ylabel('Variance (%)') #for each component
plt.title('Letters Variance By Number of Features')
plt.show()