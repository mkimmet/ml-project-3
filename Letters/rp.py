#https://www.kaggle.com/vipulgandhi/gaussian-mixture-models-clustering-explained
'''
Some code from
Patel, A. A. (2018). Hands-On Unsupervised Learning Using Python: How to Build Applied Machine Learning Solutions from Unlabeled Data. O'Reilly Media, Incorporated.
'''
import pandas as pd
import sys
from sklearn.mixture import GaussianMixture
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn import datasets
from sklearn import metrics
from scipy.spatial.distance import cdist
import numpy as np
from sklearn import preprocessing
import seaborn as sns; sns.set()
from sklearn.metrics import silhouette_score
from sklearn.random_projection import GaussianRandomProjection, SparseRandomProjection

def SelBest(arr:list, X:int)->list:
    '''
    returns the set of X configurations with shorter distance
    '''
    dx=np.argsort(arr)[:X]
    return arr[dx]

def reconstructionError(originalDF, reducedDF):
    loss = np.sum((np.array(originalDF)-np.array(reducedDF))**2, axis=1)
    loss = pd.Series(data=loss,index=originalDF.index)
    loss = (loss-np.min(loss))/(np.max(loss)-np.min(loss))
    return np.sum(loss)

file = './training-80P.csv'

cols = pd.read_csv(file, nrows=1).columns
numcols = len(cols)
#print (numcols)
features = pd.read_csv(file, usecols=cols[:-1])
#print (phish_data)
targets = pd.read_csv(file, usecols=[numcols-1])
#print (phish_target)

features_labels = ['x-box','y-box',
'width',
'height',
'onpix',
'x-bar',
'y-bar',
'x2bar',
'y2bar',
'xybar',
'x2ybar',
'xy2bar',
'x-ege',
'xegvy',
'y-ege',
'yegvx']

target_labels=['A', 'B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']

scaled_features = pd.DataFrame(preprocessing.scale(features),columns = features_labels) 

#https://github.com/vlavorini/ClusterCardinality/blob/master/Cluster%20Cardinality.ipynb
#https://www.oreilly.com/library/view/hands-on-unsupervised-learning/9781492035633/ch04.html
n_components = 1
eps = None

comps = range(1,17)
recons=[]
iterations=20
for n in comps:
    tmp_recons=[]
    for _ in range(iterations):
        GRP = GaussianRandomProjection(n_components=n, eps=eps)
        X_fit_GRP = GRP.fit(scaled_features)
        X_train_GRP = GRP.fit_transform(scaled_features)
        tmp_recons.append(reconstructionError(scaled_features,np.dot(X_train_GRP,np.linalg.pinv(GRP.components_.T))))
    val=np.mean(tmp_recons)
    recons.append(val)
    print(val)

# Plot
plt.plot(comps, recons, 'bx-')
plt.xlabel('Components')
plt.ylabel('Reconstruction Error')
plt.title('Letters RP')
plt.show()
