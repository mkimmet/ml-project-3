#USED FOR KMEANS ELBOW
import pandas as pd
import sys
from sklearn.cluster import MiniBatchKMeans, KMeans
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn import datasets
from sklearn import metrics
from scipy.spatial.distance import cdist
import numpy as np
from sklearn import preprocessing

file = './training-80P.csv'

cols = pd.read_csv(file, nrows=1).columns
numcols = len(cols)
#print (numcols)
features = pd.read_csv(file, usecols=cols[:-1])
#print (phish_data)
targets = pd.read_csv(file, usecols=[numcols-1])
#print (phish_target)

features_labels = ['x-box','y-box',
'width',
'height',
'onpix',
'x-bar',
'y-bar',
'x2bar',
'y2bar',
'xybar',
'x2ybar',
'xy2bar',
'x-ege',
'xegvy',
'y-ege',
'yegvx']

target_labels=['A', 'B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']

scaled_features = pd.DataFrame(preprocessing.scale(features),columns = features_labels) 

#https://pythonprogramminglanguage.com/kmeans-elbow-method/
distortions = []
K = range(1,10)
for k in K:
    kmeanModel = KMeans(n_clusters=k).fit(scaled_features)
    kmeanModel.fit(scaled_features)
    distortions.append(sum(np.min(cdist(scaled_features, kmeanModel.cluster_centers_, 'euclidean'), axis=1)) / scaled_features.shape[0])

# Plot the elbow
plt.plot(K, distortions, 'bx-')
plt.xlabel('k clusters')
plt.ylabel('Distortion')
plt.title('Letters Data (Scaled) Elbow Chart')
plt.show()

