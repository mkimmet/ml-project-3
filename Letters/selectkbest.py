#https://scikit-learn.org/stable/modules/generated/sklearn.feature_selection.SelectKBest.html
import pandas as pd
import sys
from sklearn.mixture import GaussianMixture
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn import datasets
from sklearn import metrics
from scipy.spatial.distance import cdist
import numpy as np
from sklearn import preprocessing
import seaborn as sns; sns.set()
from sklearn.metrics import silhouette_score
from sklearn.random_projection import GaussianRandomProjection, SparseRandomProjection
from sklearn.feature_selection import SelectKBest, chi2

def reconstructionError(originalDF, reducedDF):
    loss = np.sum((np.array(originalDF)-np.array(reducedDF))**2, axis=1)
    loss = pd.Series(data=loss,index=originalDF.index)
    loss = (loss-np.min(loss))/(np.max(loss)-np.min(loss))
    return np.sum(loss)

file = './training-80P.csv'

cols = pd.read_csv(file, nrows=1).columns
numcols = len(cols)
#print (numcols)
features = pd.read_csv(file, usecols=cols[:-1])
#print (phish_data)
targets = pd.read_csv(file, usecols=[numcols-1])
#print (phish_target)

features_labels = ['x-box','y-box',
'width',
'height',
'onpix',
'x-bar',
'y-bar',
'x2bar',
'y2bar',
'xybar',
'x2ybar',
'xy2bar',
'x-ege',
'xegvy',
'y-ege',
'yegvx']

target_labels=['A', 'B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']

scaled_features = pd.DataFrame(preprocessing.scale(features),columns = features_labels) 

#https://github.com/vlavorini/ClusterCardinality/blob/master/Cluster%20Cardinality.ipynb
#https://www.oreilly.com/library/view/hands-on-unsupervised-learning/9781492035633/ch04.html

comps = range(1,17)
recons=[]
iterations=5
for n in comps:
    tmp_recons=[]
    for _ in range(iterations):
        sk = SelectKBest(chi2, k=n)
        X_new = sk.fit_transform(features, targets)
        tmp_recons.append(reconstructionError(features,sk.inverse_transform(X_new)))
    val=np.mean(tmp_recons)
    recons.append(val)
    print(val)

# Plot 
plt.plot(comps, recons, 'bx-')
plt.xlabel('Components')
plt.ylabel('Reconstruction Error')
plt.title('Letters selectkbest')
plt.show()
