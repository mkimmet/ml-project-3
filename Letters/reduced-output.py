#USED FOR OUTPUTTING DATA

import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
# load make_blobs to simulate data
from sklearn.datasets import make_blobs
# load decomposition to do PCA analysis with sklearn
from sklearn import decomposition
from sklearn import preprocessing

from sklearn.decomposition import PCA
from sklearn.preprocessing import MinMaxScaler
from sklearn.cluster import MiniBatchKMeans, KMeans
from scipy.spatial.distance import cdist
from sklearn.metrics import silhouette_score
from sklearn.mixture import GaussianMixture
from sklearn import metrics
from sklearn.decomposition import FastICA
from sklearn.random_projection import GaussianRandomProjection, SparseRandomProjection
from sklearn.cluster import MiniBatchKMeans, KMeans
from sklearn.mixture import GaussianMixture
from sklearn.feature_selection import SelectKBest, chi2

file = './training-80P.csv'
file_test = './training-Test-20P.csv'
cols = pd.read_csv(file, nrows=1,header=None).columns
numcols = len(cols)
#print (numcols)
features = pd.read_csv(file, usecols=cols[:-1],header=None)
print (features)
targets = pd.read_csv(file, usecols=[numcols-1],header=None)
#print (phish_target)

cols_test = pd.read_csv(file_test, nrows=1).columns
numcols_test = len(cols_test)
test_features = pd.read_csv(file_test, usecols=cols_test[:-1])
#print (phish_data)
test_targets = pd.read_csv(file_test, usecols=[numcols_test-1])

features_labels = ['x-box','y-box',
'width',
'height',
'onpix',
'x-bar',
'y-bar',
'x2bar',
'y2bar',
'xybar',
'x2ybar',
'xy2bar',
'x-ege',
'xegvy',
'y-ege',
'yegvx']

scaled_features = pd.DataFrame(preprocessing.scale(features),columns = features_labels) 

##KMEANS##
kmeanModel = KMeans(n_clusters=3).fit(features)
reduced0 = kmeanModel.transform(features)
kmeans_test = kmeanModel.transform(test_features)
technique = "KMEANS"
#reduced_targets0 = np.append(reduced0,targets,axis=1)
#reduced_targets0 = np.insert(reduced0,0,np.array(['C1','C2','C3']),0)
np.savetxt(technique+".csv", reduced0, delimiter=",", fmt="%s")
#reduced_test0 = np.append(kmeans_test,test_targets,axis=1)
#reduced_test0 = np.insert(kmeans_test,0,np.array(['C1','C2','C3']),0)
np.savetxt(technique+"-test.csv", kmeans_test, delimiter=",", fmt="%s")

##EM##
gmm=GaussianMixture(4, n_init=2).fit(features) 
reduced = gmm.predict(features)
gmm_test = gmm.predict(test_features)
technique = "EM"
#reduced_targets = np.append(reduced,targets,axis=1)
#reduced_targets = np.insert(reduced,0,np.array(['C1','C2','C3']),0)
np.savetxt(technique+".csv", reduced, delimiter=",", fmt="%s")
#reduced_test = np.append(kmeans_test,test_targets,axis=1)
#reduced_test = np.insert(gmm_test,0,np.array(['C1','C2','C3']),0)
np.savetxt(technique+"-test.csv", gmm_test, delimiter=",", fmt="%s")

#UNCOMMENT THIS OUT TO DO RP AND COMMENT OUT OTHERS
#SelectKBest#
sk = SelectKBest(chi2, k=8)
sk_fit = sk.fit(features, targets)
reducedk = sk.transform(features)
sk_test = sk.transform(test_features)
technique = "SelectKBest"
reduced_targetsk = np.append(reducedk,targets,axis=1)
reduced_targetsk = np.insert(reduced_targetsk,0,np.array(['C1','C2','C3','C4','C5','C6','C7','C8','Letter']),0)
np.savetxt(technique+".csv", reduced_targetsk, delimiter=",", fmt="%s")
reduced_testk = np.append(sk_test,test_targets,axis=1)
reduced_testk = np.insert(reduced_testk,0,np.array(['C1','C2','C3','C4','C5','C6','C7','C8','Letter']),0)
np.savetxt(technique+"-test.csv", reduced_testk, delimiter=",", fmt="%s")

#UNCOMMENT THIS OUT TO DO RP AND COMMENT OUT OTHERS
#RP#
GRP = GaussianRandomProjection(n_components=15, eps=None)
grp_fit = GRP.fit(features)
reduced1 = GRP.transform(features)
grp_test = GRP.transform(test_features)
technique = "RP"
reduced_targets1 = np.append(reduced1,targets,axis=1)
reduced_targets1 = np.insert(reduced_targets1,0,np.array(['C1','C2','C3','C4','C5','C6','C7','C8','C9','C10','C11','C12','C13','C14','C15','Letter']),0)
np.savetxt(technique+".csv", reduced_targets1, delimiter=",", fmt="%s")
reduced_test1 = np.append(grp_test,test_targets,axis=1)
reduced_test1 = np.insert(reduced_test1,0,np.array(['C1','C2','C3','C4','C5','C6','C7','C8','C9','C10','C11','C12','C13','C14','C15','Letter']),0)
np.savetxt(technique+"-test.csv", reduced_test1, delimiter=",", fmt="%s")

#UNCOMMENT THIS OUT TO DO ICA AND COMMENT OUT OTHERS
##### ICA ########
ICA = FastICA(n_components=8)
ica_fit = ICA.fit(features)
reduced2 = ICA.transform(features)
ica_test = ICA.transform(test_features)
technique = "ICA"
reduced_targets2 = np.append(reduced2,targets,axis=1)
reduced_targets2 = np.insert(reduced_targets2,0,np.array(['C1','C2','C3','C4','C5','C6','C7','C8','Letter']),0)
np.savetxt(technique+".csv", reduced_targets2, delimiter=",", fmt="%s")
reduced_test2 = np.append(ica_test,test_targets,axis=1)
reduced_test2 = np.insert(reduced_test2,0,np.array(['C1','C2','C3','C4','C5','C6','C7','C8','Letter']),0)
np.savetxt(technique+"-test.csv", reduced_test2, delimiter=",", fmt="%s")

#UNCOMMENT THIS OUT TO DO PCA AND COMMENT OUT OTHERS
##### PCA ########

#Fitting the PCA algorithm with our Data
#pca = PCA().fit(features)
pca = PCA(n_components=10)
pca_fit = pca.fit(features)
reduced3 = pca.transform(features)
pca_test = pca.transform(test_features)
technique = "PCA"
reduced_targets3 = np.append(reduced3,targets,axis=1)
reduced_targets3 = np.insert(reduced_targets3,0,np.array(['C1','C2','C3','C4','C5','C6','C7','C8','C9','C10','Letter']),0)
np.savetxt(technique+".csv", reduced_targets3, delimiter=",", fmt="%s")
reduced_test3 = np.append(pca_test,test_targets,axis=1)
reduced_test3 = np.insert(reduced_test3,0,np.array(['C1','C2','C3','C4','C5','C6','C7','C8','C9','C10','Letter']),0)
np.savetxt(technique+"-test.csv", reduced_test3, delimiter=",", fmt="%s")