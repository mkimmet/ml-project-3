#USED TO FOR CLUSTERING 

import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
# load make_blobs to simulate data
from sklearn.datasets import make_blobs
# load decomposition to do PCA analysis with sklearn
from sklearn import decomposition
from sklearn import preprocessing

from sklearn.decomposition import PCA
from sklearn.preprocessing import MinMaxScaler
from sklearn.cluster import MiniBatchKMeans, KMeans
from scipy.spatial.distance import cdist
from scipy.spatial.distance import cdist
from sklearn.metrics import silhouette_score
from sklearn.mixture import GaussianMixture
from sklearn import metrics
from sklearn.decomposition import FastICA
from sklearn.random_projection import GaussianRandomProjection, SparseRandomProjection
from sklearn.feature_selection import SelectKBest, chi2

file = './phishing-80P.csv'
cols = pd.read_csv(file, nrows=1).columns
numcols = len(cols)
#print (numcols)
features = pd.read_csv(file, usecols=cols[:-1])
#print (phish_data)
targets = pd.read_csv(file, usecols=[numcols-1])
#print (phish_target)

def SelBest(arr:list, X:int)->list:
    '''
    returns the set of X configurations with shorter distance
    '''
    dx=np.argsort(arr)[:X]
    return arr[dx]


#UNCOMMENT THIS OUT TO DO SELECTKBEST AND COMMENT OUT OTHERS
scaled_features = features.replace(-1,2)
sk = SelectKBest(chi2, k=24)
reduced = sk.fit_transform(scaled_features, targets)
technique = "SelectKBest"

'''
#UNCOMMENT THIS OUT TO DO RP AND COMMENT OUT OTHERS

GRP = GaussianRandomProjection(n_components=29, eps=None)
reduced = GRP.fit_transform(features)
technique = "RP"
'''
''' 
#UNCOMMENT THIS OUT TO DO ICA AND COMMENT OUT OTHERS

##### ICA ########
ICA = FastICA(n_components=26) 
reduced=ICA.fit_transform(features)
technique = "ICA"


#UNCOMMENT THIS OUT TO DO PCA AND COMMENT OUT OTHERS

##### PCA ########

#Fitting the PCA algorithm with our Data
#pca = PCA().fit(features)
pca = PCA(n_components=10)
reduced = pca.fit_transform(features)
#np.savetxt("foo.csv", dataset, delimiter=",")
technique = "PCA"
'''


##### EM ########
#https://github.com/vlavorini/ClusterCardinality/blob/master/Cluster%20Cardinality.ipynb
n_clusters=np.arange(2, 10)
sils=[]
sils_err=[]
iterations=5
for n in n_clusters:
    tmp_sil=[]
    for _ in range(iterations):
        gmm=GaussianMixture(n, n_init=2).fit(reduced) 
        labels=gmm.predict(reduced)
        sil=metrics.silhouette_score(reduced, labels, metric='euclidean')
        tmp_sil.append(sil)
    val=np.mean(SelBest(np.array(tmp_sil), int(iterations/1)))
    err=np.std(tmp_sil)
    sils.append(val)
    sils_err.append(err)
    print(n)
    print(tmp_sil)
    print(val)
    print(err)

plt.errorbar(n_clusters, sils, yerr=sils_err)
plt.title("Phishing " + technique + " Reduced EM")
plt.xticks(n_clusters)
plt.xlabel("Clusters")
plt.ylabel("Score")
plt.show()



###### KMEANS ########
#https://pythonprogramminglanguage.com/kmeans-elbow-method/
distortions = []
K = range(1,19)
for k in K:
    kmeanModel = KMeans(n_clusters=k).fit(reduced)
    kmeanModel.fit(reduced)
    distortions.append(sum(np.min(cdist(reduced, kmeanModel.cluster_centers_, 'euclidean'), axis=1)) / reduced.shape[0])

# Plot the elbow
plt.plot(K, distortions, 'bx-')
plt.xlabel('k clusters')
plt.ylabel('Distortion')
plt.title('Phishing Data ' + technique + ' Reduced KMeans')
plt.show()
