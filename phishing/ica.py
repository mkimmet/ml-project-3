#USED FOR ICA 
#https://pythonprogramminglanguage.com/kmeans-elbow-method/
import pandas as pd
import sys
from sklearn.cluster import MiniBatchKMeans, KMeans
from sklearn.decomposition import FastICA
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn import datasets
from sklearn import metrics
from scipy.spatial.distance import cdist
import numpy as np
from scipy.stats import kurtosis

file = './phishing-80P.csv'

cols = pd.read_csv(file, nrows=1).columns
numcols = len(cols)
#print (numcols)
features = pd.read_csv(file, usecols=cols[:-1])
#print (phish_data)
targets = pd.read_csv(file, usecols=[numcols-1])
#print (phish_target)

feature_labels = ['having_IP_Address','URL_Length','Shortining_Service',
'having_At_Symbol',
'double_slash_redirecting',
'Prefix_Suffix',
'having_Sub_Domain',
'SSLfinal_State',
'Domain_registeration_length',
'Favicon',
'port',
'HTTPS_token',
'Request_URL',
'URL_of_Anchor',
'Links_in_tags',
'SFH',
'Submitting_to_email',
'Abnormal_URL',
'Redirect',
'on_mouseover',
'RightClick',
'popUpWidnow',
'Iframe',
'age_of_domain',
'DNSRecord',
'web_traffic',
'Page_Rank',
'Google_Index',
'Links_pointing_to_page',
'Statistical_report']

target_labels=['Not Phishing', 'Phishing']

feats = range(1,31)
kurts=[]
iterations=5
for n in feats:
    tmp_kurt=[]
    for _ in range(iterations):
        ica = FastICA(n_components=n) 
        X=ica.fit_transform(features)
        tmp_kurt.append(np.mean(kurtosis(X)**2))
    val=np.mean(tmp_kurt)
    kurts.append(val)
    print(val)

# Plot the elbow
plt.plot(feats, kurts, 'bx-')
plt.xlabel('Features')
plt.ylabel('Kurtosis')
plt.title('Phishing Data Kurtosis')
plt.show()