#https://scikit-learn.org/stable/modules/generated/sklearn.feature_selection.SelectKBest.html
import pandas as pd
import sys
from sklearn.mixture import GaussianMixture
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn import datasets
from sklearn import metrics
from scipy.spatial.distance import cdist
import numpy as np
from sklearn import preprocessing
import seaborn as sns; sns.set()
from sklearn.metrics import silhouette_score
from sklearn.random_projection import GaussianRandomProjection, SparseRandomProjection
from sklearn.feature_selection import SelectKBest, chi2

def reconstructionError(originalDF, reducedDF):
    loss = np.sum((np.array(originalDF)-np.array(reducedDF))**2, axis=1)
    loss = pd.Series(data=loss,index=originalDF.index)
    loss = (loss-np.min(loss))/(np.max(loss)-np.min(loss))
    return np.sum(loss)

file = './phishing-80P.csv'

cols = pd.read_csv(file, nrows=1).columns
numcols = len(cols)
#print (numcols)
features = pd.read_csv(file, usecols=cols[:-1])
#print (phish_data)
targets = pd.read_csv(file, usecols=[numcols-1])
#print (phish_target)

feature_labels = ['having_IP_Address','URL_Length','Shortining_Service',
'having_At_Symbol',
'double_slash_redirecting',
'Prefix_Suffix',
'having_Sub_Domain',
'SSLfinal_State',
'Domain_registeration_length',
'Favicon',
'port',
'HTTPS_token',
'Request_URL',
'URL_of_Anchor',
'Links_in_tags',
'SFH',
'Submitting_to_email',
'Abnormal_URL',
'Redirect',
'on_mouseover',
'RightClick',
'popUpWidnow',
'Iframe',
'age_of_domain',
'DNSRecord',
'web_traffic',
'Page_Rank',
'Google_Index',
'Links_pointing_to_page',
'Statistical_report']

target_labels=['Not Phishing', 'Phishing']

#https://github.com/vlavorini/ClusterCardinality/blob/master/Cluster%20Cardinality.ipynb
#https://www.oreilly.com/library/view/hands-on-unsupervised-learning/9781492035633/ch04.html
scaled_features = features.replace(-1,2) 

comps = range(1,31)
recons=[]
iterations=5
for n in comps:
    tmp_recons=[]
    for _ in range(iterations):
        sk = SelectKBest(chi2, k=n)
        X_new = sk.fit_transform(scaled_features, targets)
        print(sk.inverse_transform(X_new))
        tmp_recons.append(reconstructionError(scaled_features,sk.inverse_transform(X_new)))
    val=np.mean(tmp_recons)
    recons.append(val)
    print(val)

# Plot
plt.plot(comps, recons, 'bx-')
plt.xlabel('Components')
plt.ylabel('Reconstruction Error')
plt.title('Phishing selectkbest')
plt.show()
