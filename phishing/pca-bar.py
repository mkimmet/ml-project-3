#USED TO GENERATE THE PCA BAR CHART
#https://cmdlinetips.com/2018/03/pca-example-in-python-with-scikit-learn/

import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
from sklearn.datasets import make_blobs
from sklearn import decomposition

file = './phishing-80P.csv'

cols = pd.read_csv(file, nrows=1).columns
numcols = len(cols)
#print (numcols)
phish_features = pd.read_csv(file, usecols=cols[:-1])
#print (phish_data)
phish_target = pd.read_csv(file, usecols=[numcols-1])
#print (phish_target)

phish_features_names = ['having_IP_Address','URL_Length','Shortining_Service',
'having_At_Symbol',
'double_slash_redirecting',
'Prefix_Suffix',
'having_Sub_Domain',
'SSLfinal_State',
'Domain_registeration_length',
'Favicon',
'port',
'HTTPS_token',
'Request_URL',
'URL_of_Anchor',
'Links_in_tags',
'SFH',
'Submitting_to_email',
'Abnormal_URL',
'Redirect',
'on_mouseover',
'RightClick',
'popUpWidnow',
'Iframe',
'age_of_domain',
'DNSRecord',
'web_traffic',
'Page_Rank',
'Google_Index',
'Links_pointing_to_page',
'Statistical_report']

targets = ['Not Phishing', 'Phishing']


X1, Y1 = make_blobs(n_features=10, 
         n_samples=100,
         centers=4, random_state=4,
         cluster_std=2)
print(phish_features.shape)
print(X1.shape)


pca = decomposition.PCA(n_components=30) 

pc = pca.fit_transform(phish_features)

pc_df = pd.DataFrame(data = pc ,
        columns = ['PC1', 'PC2','PC3','PC4','PC5','PC6','PC7','PC8','PC9','PC10','PC11','PC12','PC13','PC14','PC15','PC16','PC17','PC18','PC19','PC20','PC21','PC22','PC23','PC24','PC25','PC26','PC27','PC28','P29','PC30'])
pc_df['Cluster'] = phish_target
pc_df.head()

df = pd.DataFrame({'var':pca.explained_variance_ratio_,
             'PC':['P1', 'P2','P3','P4','P5','P6','P7','P8','P9','P10','P11','P12','P13','P14','P15','P16','P17','P18','P19','P20','P21','P22','P23','P24','P25','P26','P27','P28','P29','P30']})
sns.barplot(x='PC',y="var", 
           data=df, color="c")

sns.lmplot( x="PC1", y="PC2",
  data=pc_df, 
  fit_reg=False, 
  hue='Cluster',
  legend=True,
  scatter_kws={"s": 80})

plt.show()