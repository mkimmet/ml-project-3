#USED FOR OUTPUTTING DATA

import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
# load make_blobs to simulate data
from sklearn.datasets import make_blobs
# load decomposition to do PCA analysis with sklearn
from sklearn import decomposition
from sklearn import preprocessing

from sklearn.decomposition import PCA
from sklearn.preprocessing import MinMaxScaler
from sklearn.cluster import MiniBatchKMeans, KMeans
from scipy.spatial.distance import cdist
from sklearn.metrics import silhouette_score
from sklearn.mixture import GaussianMixture
from sklearn import metrics
from sklearn.decomposition import FastICA
from sklearn.random_projection import GaussianRandomProjection, SparseRandomProjection

file = './phishing-80P.csv'
file_test = './phishing-Test-20P.csv'
cols = pd.read_csv(file, nrows=1).columns
numcols = len(cols)
#print (numcols)
features = pd.read_csv(file, usecols=cols[:-1])
#print (phish_data)
targets = pd.read_csv(file, usecols=[numcols-1])
#print (phish_target)

cols_test = pd.read_csv(file_test, nrows=1).columns
numcols_test = len(cols_test)
test_features = pd.read_csv(file_test, usecols=cols_test[:-1])
#print (phish_data)
test_targets = pd.read_csv(file_test, usecols=[numcols_test-1])

feature_labels = ['having_IP_Address','URL_Length','Shortining_Service',
'having_At_Symbol',
'double_slash_redirecting',
'Prefix_Suffix',
'having_Sub_Domain',
'SSLfinal_State',
'Domain_registeration_length',
'Favicon',
'port',
'HTTPS_token',
'Request_URL',
'URL_of_Anchor',
'Links_in_tags',
'SFH',
'Submitting_to_email',
'Abnormal_URL',
'Redirect',
'on_mouseover',
'RightClick',
'popUpWidnow',
'Iframe',
'age_of_domain',
'DNSRecord',
'web_traffic',
'Page_Rank',
'Google_Index',
'Links_pointing_to_page',
'Statistical_report']

target_labels=['Not Phishing', 'Phishing']

#UNCOMMENT THIS OUT TO DO RP AND COMMENT OUT OTHERS
#RP#
GRP = GaussianRandomProjection(n_components=15, eps=None)
grp_fit = GRP.fit(scaled_features)
reduced1 = GRP.transform(scaled_features)
grp_test = GRP.transform(test_features)
technique = "RP"
reduced_targets1 = np.append(reduced1,targets,axis=1)
reduced_targets1 = np.insert(reduced_targets1,0,np.array(['C1','C2','C3','C4','C5','C6','C7','C8','C9','C10','C11','C12','C13','C14','C15','Letter']),0)
np.savetxt(technique+".csv", reduced_targets1, delimiter=",", fmt="%s")
reduced_test1 = np.append(grp_test,test_targets,axis=1)
reduced_test1 = np.insert(reduced_test1,0,np.array(['C1','C2','C3','C4','C5','C6','C7','C8','C9','C10','C11','C12','C13','C14','C15','Letter']),0)
np.savetxt(technique+"-test.csv", reduced_test1, delimiter=",", fmt="%s")

#UNCOMMENT THIS OUT TO DO ICA AND COMMENT OUT OTHERS
##### ICA ########
ICA = FastICA(n_components=8)
ica_fit = ICA.fit(scaled_features)
reduced2 = ICA.transform(scaled_features)
ica_test = ICA.transform(test_features)
technique = "ICA"
reduced_targets2 = np.append(reduced2,targets,axis=1)
reduced_targets2 = np.insert(reduced_targets2,0,np.array(['C1','C2','C3','C4','C5','C6','C7','C8','Letter']),0)
np.savetxt(technique+".csv", reduced_targets2, delimiter=",", fmt="%s")
reduced_test2 = np.append(ica_test,test_targets,axis=1)
reduced_test2 = np.insert(reduced_test2,0,np.array(['C1','C2','C3','C4','C5','C6','C7','C8','Letter']),0)
np.savetxt(technique+"-test.csv", reduced_test2, delimiter=",", fmt="%s")

#UNCOMMENT THIS OUT TO DO PCA AND COMMENT OUT OTHERS
##### PCA ########

#Fitting the PCA algorithm with our Data
#pca = PCA().fit(features)
pca = PCA(n_components=18)
pca_fit = pca.fit(scaled_features)
reduced3 = pca.transform(scaled_features)
pca_test = pca.transform(test_features)
technique = "PCA"
reduced_targets3 = np.append(reduced3,targets,axis=1)
reduced_targets3 = np.insert(reduced_targets3,0,np.array(['C1','C2','C3','C4','C5','C6','C7','C8','C9','C10','C11','C12','C13','C14','C15','C16','C17','C18','phishing']),0)
np.savetxt(technique+".csv", reduced_targets3, delimiter=",", fmt="%s")
reduced_test3 = np.append(pca_test,test_targets,axis=1)
reduced_test3 = np.insert(reduced_test3,0,np.array(['C1','C2','C3','C4','C5','C6','C7','C8','C9','C10','C11','C12','C13','C14','C15','C16','C17','C18','phishing']),0)
np.savetxt(technique+"-test.csv", reduced_test3, delimiter=",", fmt="%s")
'''
##### EM ########
#https://github.com/vlavorini/ClusterCardinality/blob/master/Cluster%20Cardinality.ipynb
n_clusters=np.arange(2, 27)
sils=[]
sils_err=[]
iterations=5
for n in n_clusters:
    tmp_sil=[]
    for _ in range(iterations):
        gmm=GaussianMixture(n, n_init=2).fit(reduced) 
        labels=gmm.predict(reduced)
        sil=metrics.silhouette_score(reduced, labels, metric='euclidean')
        tmp_sil.append(sil)
    val=np.mean(SelBest(np.array(tmp_sil), int(iterations/1)))
    err=np.std(tmp_sil)
    sils.append(val)
    sils_err.append(err)
    print(n)
    print(tmp_sil)
    print(val)
    print(err)

plt.errorbar(n_clusters, sils, yerr=sils_err)
plt.title("Letters " + technique + " Reduced EM")
plt.xticks(n_clusters)
plt.xlabel("Clusters")
plt.ylabel("Score")
plt.show()




##### KMeans ########
#https://pythonprogramminglanguage.com/kmeans-elbow-method/
distortions = []
K = range(1,27)
for k in K:
    kmeanModel = KMeans(n_clusters=k).fit(reduced)
    kmeanModel.fit(reduced)
    distortions.append(sum(np.min(cdist(reduced, kmeanModel.cluster_centers_, 'euclidean'), axis=1)) / reduced.shape[0])

# Plot the elbow
plt.plot(K, distortions, 'bx-')
plt.xlabel('k clusters')
plt.ylabel('Distortion')
plt.title('Letters Data ' + technique + ' Reduced KMeans')
plt.show()
'''