#https://www.kaggle.com/vipulgandhi/gaussian-mixture-models-clustering-explained
'''
Some code from:
Patel, A. A. (2018). Hands-On Unsupervised Learning Using Python: How to Build Applied Machine Learning Solutions from Unlabeled Data. O'Reilly Media, Incorporated.
'''

import pandas as pd
import sys
from sklearn.cluster import MiniBatchKMeans, KMeans
from sklearn.decomposition import FastICA
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn import datasets
from sklearn import metrics
from scipy.spatial.distance import cdist
import numpy as np
from scipy.stats import kurtosis
from sklearn.random_projection import GaussianRandomProjection, SparseRandomProjection

file = './phishing-80P.csv'

cols = pd.read_csv(file, nrows=1).columns
numcols = len(cols)
#print (numcols)
features = pd.read_csv(file, usecols=cols[:-1])
#print (phish_data)
targets = pd.read_csv(file, usecols=[numcols-1])
#print (phish_target)

def reconstructionError(originalDF, reducedDF):
    loss = np.sum((np.array(originalDF)-np.array(reducedDF))**2, axis=1)
    loss = pd.Series(data=loss,index=originalDF.index)
    loss = (loss-np.min(loss))/(np.max(loss)-np.min(loss))
    return np.sum(loss)

feature_labels = ['having_IP_Address','URL_Length','Shortining_Service',
'having_At_Symbol',
'double_slash_redirecting',
'Prefix_Suffix',
'having_Sub_Domain',
'SSLfinal_State',
'Domain_registeration_length',
'Favicon',
'port',
'HTTPS_token',
'Request_URL',
'URL_of_Anchor',
'Links_in_tags',
'SFH',
'Submitting_to_email',
'Abnormal_URL',
'Redirect',
'on_mouseover',
'RightClick',
'popUpWidnow',
'Iframe',
'age_of_domain',
'DNSRecord',
'web_traffic',
'Page_Rank',
'Google_Index',
'Links_pointing_to_page',
'Statistical_report']

target_labels=['Not Phishing', 'Phishing']

comps = range(1,31)
recons=[]
iterations=20
for n in comps:
    tmp_recons=[]
    for _ in range(iterations):
        GRP = GaussianRandomProjection(n_components=n, eps=None)
        X_fit_GRP = GRP.fit(features)
        X_train_GRP = GRP.fit_transform(features)
        tmp_recons.append(reconstructionError(features,np.dot(X_train_GRP,np.linalg.pinv(GRP.components_.T))))
    val=np.mean(tmp_recons)
    recons.append(val)
    print(val)

print(np.dot(X_train_GRP,np.linalg.pinv(GRP.components_.T)))

# Plot
plt.plot(comps, recons, 'bx-')
plt.xlabel('Components')
plt.ylabel('Reconstruction Error')
plt.title('Phishing RP')
plt.show()